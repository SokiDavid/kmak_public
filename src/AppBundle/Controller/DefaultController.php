<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $galleries = $em->getRepository('AppBundle:Gallery')->findAll();

        return $this->render('default/index.html.twig', [
            'galleries' => $galleries,
        ]);
    }

    public function galleryAction($slug)
    {
        $em = $this->getDoctrine()->getManager();

        $gallery = $em->getRepository('AppBundle:Gallery')->findOneBy(
            [ 'slug' => $slug ]
        );

        if (!$gallery) {
            throw $this->createNotFoundException('The gallery does not exist');
        }

        $pictures = $em->getRepository('AppBundle:Picture')->findBy(
            [ 'gallery' => $gallery ]
        );

        return $this->render('gallery/gallery.html.twig', [
            'gallery' => $gallery,
            'pictures' => $pictures,
        ]);
    }
}
