$(document).ready(function () {
    getDateNow();

    setInterval(function () {
        getDateNow();
    }, 1000);
});

function getDateNow() {
    var date = new Date();

    hours = date.getHours();
    minutes = addZero(date.getMinutes());
    seconds = addZero(date.getSeconds());

    $('.date_now').text(hours + ':' + minutes + ':' + seconds);
}

function addZero(i) {
    if (i < 10) {
        i = '0' + i;
    }

    return i;
}